const express = require('express')
const Connection = require('mysql/lib/Connection')
const { NULL } = require('mysql/lib/protocol/constants/types')
const db = require('../db')
const utils = require('../utils')


const router = express.Router()


router.post('/add', (request,response) => {

    const { name,sclass,division,dob,phone,password } = request.body

    const connection = db.openConnection()

    const nameStatement = `SELECT name FROM student WHERE name = '${name}'`

    connection.query(nameStatement, (error, names) => {
        if (error) {
            connection.end()
            response.send(utils.createResult(error))
        }
        else {
            if (names.length == 0) {
            
                const statement = `INSERT INTO student (name, class, division, dateofbirth,parent_mobile_no,password) VALUES ('${name}','${sclass}','${division}','${dob}','${phone}','${password}')`
              connection.query(statement, (error, result) => {
                connection.end()
                response.send(utils.createResult(error, result))
              })
            } else {
              // at least one user exists with this email address
              connection.end()
              response.send(
                utils.createResult('name already exists, please use another')
              )
            }
          }
    })
})

router.post('/signin', (request, response) => {
    const { rollno, password} = request.body

    const connection = db.openConnection()

    const statement = `SELECT name, class FROM student WHERE roll_no = '${rollno}' AND password = '${password}'`

    connection.query(statement, (error, users) => {
        if (error) {
            response.send(utils.createResult(error))
        } else if (users.length == 0) {
            // there is no user matching the criteria
            response.send(utils.createResult('user not found'))
        } else {
            const user = users[0]
            response.send(utils.createResult(null, user))
        }
    })
})




router.put('/edit/:id', (request, response) => {
    const { id } =request.params
    const { sclass, division} = request.body

    const statement = `UPDATE student SET class = '${sclass}', division = '${division}' WHERE roll_no = '${id}'`

    const connection = db.openConnection()

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (request, response) => {
    const { id } = request.params

    const statement = `DELETE FROM student WHERE roll_no = '${id}'`

    const connection = db.openConnection()

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

//Fetch all students
router.get('/search', (request, response) => {
    // async function is needed to use await inside it
    const searchStudents = () => {
      const { rollno, sclass } = request.query
  
      const conditions = []
  
      // check if user parameter exists in query string
      // /blog/search?user=1
      if (rollno) {
        conditions.push(` roll_no = ${rollno} `)
      }
  
      // check if title parameter exists in query string
      // /blog/search?title=react
      if (sclass) {
        conditions.push(` class = ${sclass} `)
      }

  
      let where = ''
      if (conditions.length > 0) {
        where = ` WHERE ${conditions.join(' and ')} `
      }
  
      const statement = `
        select * from student ${where}`
  
      // wait till mysql connection is open
      const connection = db.openConnection()
  
      // wait till the query gets executed
      //const [students] = connection.query(statement)

      
  
      //connection.end()
      //response.send(utils.createResult(null, students))
      connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
    }
  
    searchStudents()
  })

  router.post('/searchAll', (request, response) => {
    // async function is needed to use await inside it
    const searchAllStudents = () => {
      //const { rollno, sclass } = request.query
  
      //const conditions = []
  
      // check if user parameter exists in query string
      // /blog/search?user=1
      //if (rollno) {
      //  conditions.push(` roll_no = ${rollno} `)
     // }
  
      // check if title parameter exists in query string
      // /blog/search?title=react
      //if (sclass) {
       // conditions.push(` class = ${sclass} `)
     // }

  
     // let where = ''
      //if (conditions.length > 0) {
        //where = ` WHERE ${conditions.join(' and ')} `
      //}
  
      const statement = `
        select * from student`
  
      // wait till mysql connection is open
      const connection = db.openConnection()
  
      // wait till the query gets executed
      //const [students] = connection.query(statement)

      
  
      //connection.end()
      //response.send(utils.createResult(null, students))
      connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
    }
  
    searchAllStudents()
  })


module.exports = router